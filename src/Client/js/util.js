// @ts-check

import lzString from "lz-string";

export function compressToEncodedURIComponent(x) {
    return lzString.compressToEncodedURIComponent(x);
}

export function decompressFromEncodedURIComponent(x) {
    return lzString.decompressFromEncodedURIComponent(x);
}

export function getURIhash() {
    return window.location.hash.replace(/^\#\?/, '');
}

export function setURIhash(x) {
    window.location.hash = '?' + x;
}