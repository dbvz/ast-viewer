module AstTransformer

open Shared
open Microsoft.FSharp.Compiler.Ast

module Helpers =
    let r(r: Microsoft.FSharp.Compiler.Range.range): Range option =
        Some
            {StartLine = r.StartLine
             StartCol = r.StartColumn
             EndLine = r.EndLine
             EndCol = r.EndColumn}

    let p = Map.ofList
    let inline (==>) a b = (a,box b)

    let noRange =
        None

    let i (id: Ident) : Id =
        { Ident = id.idText
          Range = r id.idRange}

    let li (id: LongIdent) =
        id |> List.map i

    let lid (id: LongIdentWithDots) = li id.Lid

module private Ast =
    open Helpers

    let rec visit(ast: SynModuleDecl): Node =
        match ast with
        | SynModuleDecl.ModuleAbbrev(ident,longIdent,range) ->
            {Type = "SynModuleDecl.ModuleAbbrev"
             Range = r range
             Properties =
                 p ["ident" ==> i ident
                    "longIdent" ==> li longIdent]
             Childs = []}
        | SynModuleDecl.NestedModule(sci,isRecursive,decls,_,range) ->
            {Type = "SynModuleDecl.NestedModule"
             Range = r range
             Properties = p ["isRecursive" ==> isRecursive]
             Childs =
                 [yield visitSynComponentInfo sci
                  yield! (decls |> List.map visit)]}
        | SynModuleDecl.Let(_,bindings,range) ->
            {Type = "SynModuleDecl.Let"
             Range = r range
             Properties = p []
             Childs = bindings |> List.map visitSynBinding}
        | SynModuleDecl.DoExpr(_,expr,range) ->
            {Type = "SynModuleDecl.DoExpr"
             Range = r range
             Properties = p []
             Childs = [visitSynExpr expr]}
        | SynModuleDecl.Types(typeDefs,range) ->
            {Type = "SynModuleDecl.Types"
             Range = r range
             Properties = p []
             Childs = typeDefs |> List.map visitSynTypeDefn}
        | SynModuleDecl.Exception(exceptionDef,range) ->
            {Type = "SynModuleDecl.Exception"
             Range = r range
             Properties = p []
             Childs = [visitSynExceptionDefn exceptionDef]}
        | SynModuleDecl.Open(longDotId,range) ->
            {Type = "SynModuleDecl.Open"
             Range = r range
             Properties = p ["longIdent" ==> lid longDotId]
             Childs = []}
        | SynModuleDecl.Attributes(attrs,range) ->
            {Type = "SynModuleDecl.Attributes"
             Range = r range
             Properties = p []
             Childs = attrs |> List.map visitSynAttribute}
        | SynModuleDecl.HashDirective(hash,range) ->
            {Type = "SynModuleDecl.HashDirective"
             Range = r range
             Properties = p []
             Childs = [visitParsedHashDirective hash]}
        | SynModuleDecl.NamespaceFragment(moduleOrNamespace) ->
            {Type = "SynModuleDecl.NamespaceFragment"
             Range = noRange
             Properties = p []
             Childs = [visitSynModuleOrNamespace moduleOrNamespace]}

    and visitSynModuleOrNamespace(modOrNs: SynModuleOrNamespace): Node =
        match modOrNs with
        | SynModuleOrNamespace(longIdent,isRecursive,isModule,decls,_,attrs,access,range) ->
            {Type = "SynModuleOrNamespace"
             Range = r range
             Properties =
                 p [yield "isRecursive" ==> isRecursive
                    yield "isModule" ==> isModule
                    yield "longIdent" ==> li longIdent
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs =
                 [yield! attrs |> List.map visitSynAttribute
                  yield! (decls |> List.map visit)]}

    and visitSynExpr(synExpr: SynExpr): Node =
        match synExpr with
        | SynExpr.Paren(expr,leftParenRange,rightParenRange,range) ->
            {Type = "SynExpr.Paren"
             Range = r range
             Properties =
                 p [yield "leftParenRange" ==> r leftParenRange
                    if rightParenRange.IsSome then yield "rightParenRange" ==> r rightParenRange.Value]
             Childs = [yield visitSynExpr expr]}
        | SynExpr.Quote(operator,isRaw,quotedSynExpr,isFromQueryExpression,range) ->
            {Type = "SynExpr.Quote"
             Range = r range
             Properties =
                 p ["isRaw" ==> isRaw
                    "isFromQueryExpression" ==> isFromQueryExpression]
             Childs =
                 [yield visitSynExpr operator
                  yield visitSynExpr quotedSynExpr]}
        | SynExpr.Const(constant,range) ->
            {Type = "SynExpr.Const"
             Range = r range
             Properties = p ["constant" ==> visitSynConst constant]
             Childs = []}
        | SynExpr.Typed(expr,typeName,range) ->
            {Type = "SynExpr.Typed"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynExpr expr
                  yield visitSynType typeName]}
        | SynExpr.Tuple(exprs,commaRanges,range) ->
            {Type = "SynExpr.Tuple"
             Range = r range
             Properties = p ["commaRanges" ==> (commaRanges |> List.map r)]
             Childs = [yield! exprs |> List.map visitSynExpr]}
        | SynExpr.StructTuple(exprs,commaRanges,range) ->
            {Type = "SynExpr.StructTuple"
             Range = r range
             Properties = p ["commaRanges" ==> (commaRanges |> List.map r)]
             Childs = [yield! exprs |> List.map visitSynExpr]}
        | SynExpr.ArrayOrList(isList,exprs,range) ->
            {Type = "SynExpr.StructTuple"
             Range = r range
             Properties = p ["isList" ==> isList]
             Childs = [yield! exprs |> List.map visitSynExpr]}
        | SynExpr.Record(_,_,recordFields,range) ->
            {Type = "SynExpr.Record"
             Range = r range
             Properties = p []
             Childs = [yield! recordFields |> List.map visitRecordField]}
        | SynExpr.New(isProtected,typeName,expr,range) ->
            {Type = "SynExpr.New"
             Range = r range
             Properties = p ["isProtected" ==> isProtected]
             Childs =
                 [yield visitSynExpr expr
                  yield visitSynType typeName]}
        | SynExpr.ObjExpr(objType,argOptions,bindings,extraImpls,newExprRange,range) ->
            {Type = "SynExpr.ObjExpr"
             Range = r range
             Properties = p ["newExprRange" ==> r newExprRange]
             Childs =
                 [yield visitSynType objType
                  if argOptions.IsSome then yield visitArgsOption argOptions.Value
                  yield! extraImpls |> List.map visitSynInterfaceImpl
                  yield! bindings |> List.map visitSynBinding]}
        | SynExpr.While(_,whileExpr,doExpr,range) ->
            {Type = "SynExpr.While"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynExpr whileExpr
                  yield visitSynExpr doExpr]}
        | SynExpr.For(_,ident,identBody,_,toBody,doBody,range) ->
            {Type = "SynExpr.For"
             Range = r range
             Properties = p ["ident" ==> i ident]
             Childs =
                 [yield visitSynExpr identBody
                  yield visitSynExpr toBody
                  yield visitSynExpr doBody]}
        | SynExpr.ForEach(_,(SeqExprOnly seqExprOnly),isFromSource,pat,enumExpr,bodyExpr,range) ->
            {Type = "SynExpr.ForEach"
             Range = r range
             Properties =
                 p ["isFromSource" ==> isFromSource
                    "seqExprOnly" ==> seqExprOnly]
             Childs =
                 [yield visitSynPat pat
                  yield visitSynExpr enumExpr
                  yield visitSynExpr bodyExpr]}
        | SynExpr.ArrayOrListOfSeqExpr(isArray,expr,range) ->
            {Type = "SynExpr.ArrayOrListOfSeqExpr"
             Range = r range
             Properties = p ["isArray" ==> isArray]
             Childs = [yield visitSynExpr expr]}
        | SynExpr.CompExpr(isArrayOrList,isNotNakedRefCell,expr,range) ->
            {Type = "SynExpr.CompExpr"
             Range = r range
             Properties =
                 p ["isArrayOrList" ==> isArrayOrList
                    "isNotNakedRefCell" ==> isNotNakedRefCell]
             Childs = [yield visitSynExpr expr]}
        | SynExpr.Lambda(fromMethod,inLambdaSeq,args,body,range) ->
            {Type = "SynExpr.Lambda"
             Range = r range
             Properties =
                 p ["fromMethod" ==> fromMethod
                    "inLambdaSeq" ==> inLambdaSeq]
             Childs =
                 [yield visitSynSimplePats args
                  yield visitSynExpr body]}
        | SynExpr.MatchLambda(isExnMatch,_,matchClaseus,_,range) ->
            {Type = "SynExpr.MatchLambda"
             Range = r range
             Properties = p ["isExnMatch" ==> isExnMatch]
             Childs = [yield! matchClaseus |> List.map visitSynMatchClause]}
        | SynExpr.Match(_,expr,clauses,isExnMatch,range) ->
            {Type = "SynExpr.Match"
             Range = r range
             Properties = p ["isExnMatch" ==> isExnMatch]
             Childs =
                 [yield visitSynExpr expr
                  yield! clauses |> List.map visitSynMatchClause]}
        | SynExpr.Do(expr,range) ->
            {Type = "SynExpr.Do"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}
        | SynExpr.Assert(expr,range) ->
            {Type = "SynExpr.Assert"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}
        | SynExpr.App(atomicFlag,isInfix,funcExpr,argExpr,range) ->
            {Type = "SynExpr.App"
             Range = r range
             Properties =
                 p ["atomicFlag" ==> (match atomicFlag with
                                      | ExprAtomicFlag.Atomic -> "Atomic"
                                      | _ -> "Not Atomic")
                    "isInfix" ==> isInfix]
             Childs =
                 [yield visitSynExpr funcExpr
                  yield visitSynExpr argExpr]}
        | SynExpr.TypeApp(expr,lESSrange,typeNames,commaRanges,gREATERrange,typeArgsRange,range) ->
            {Type = "SynExpr.TypeApp"
             Range = r range
             Properties =
                 p [yield "lESSrange" ==> r lESSrange
                    yield "commaRanges" ==> (commaRanges |> List.map r)
                    if gREATERrange.IsSome then yield "gREATERrange" ==> r gREATERrange.Value
                    yield "typeArgsRange" ==> r typeArgsRange]
             Childs =
                 [yield visitSynExpr expr
                  yield! typeNames |> List.map visitSynType]}
        | SynExpr.LetOrUse(isRecursive,isUse,bindings,body,range) ->
            {Type = "SynExpr.LetOrUse"
             Range = r range
             Properties =
                 p ["isRecursive" ==> isRecursive
                    "isUse" ==> isUse]
             Childs =
                 [yield! bindings |> List.map visitSynBinding
                  yield visitSynExpr body]}
        | SynExpr.TryWith(tryExpr,tryRange,withCases,withRange,range,_,_) ->
            {Type = "SynExpr.TryWith"
             Range = r range
             Properties =
                 p ["tryRange" ==> r tryRange
                    "withRange" ==> r withRange]
             Childs =
                 [yield visitSynExpr tryExpr
                  yield! withCases |> List.map visitSynMatchClause]}
        | SynExpr.TryFinally(tryExpr,finallyExpr,range,_,_) ->
            {Type = "SynExpr.TryFinally"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynExpr tryExpr
                  yield visitSynExpr finallyExpr]}
        | SynExpr.Lazy(ex,range) ->
            {Type = "SynExpr.Lazy"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr ex]}
        | SynExpr.Sequential(_,isTrueSeq,expr1,expr2,range) ->
            {Type = "SynExpr.Sequential"
             Range = r range
             Properties = p ["isTrueSeq" ==> isTrueSeq]
             Childs =
                 [yield visitSynExpr expr1
                  yield visitSynExpr expr2]}
        | SynExpr.IfThenElse(ifExpr,thenExpr,elseExpr,_,isFromErrorRecovery,ifToThenRange,range) ->
            {Type = "SynExpr.IfThenElse"
             Range = r range
             Properties =
                 p ["isFromErrorRecovery" ==> isFromErrorRecovery
                    "ifToThenRange" ==> r ifToThenRange]
             Childs =
                 [yield visitSynExpr ifExpr
                  yield visitSynExpr thenExpr
                  if elseExpr.IsSome then yield visitSynExpr elseExpr.Value]}
        | SynExpr.Ident(id) ->
            {Type = "SynExpr.Ident"
             Range = noRange
             Properties = p ["ident" ==> i id]
             Childs = []}
        | SynExpr.LongIdent(isOptional,longDotId,_,range) ->
            {Type = "SynExpr.LongIdent"
             Range = r range
             Properties =
                 p ["isOptional" ==> isOptional
                    "longDotId" ==> lid longDotId]
             Childs = []}
        | SynExpr.LongIdentSet(longDotId,expr,range) ->
            {Type = "SynExpr.LongIdentSet"
             Range = r range
             Properties = p ["longDotId" ==> lid longDotId]
             Childs = [yield visitSynExpr expr]}
        | SynExpr.DotGet(expr,rangeOfDot,longDotId,range) ->
            {Type = "SynExpr.DotGet"
             Range = r range
             Properties =
                 p ["rangeOfDot" ==> r rangeOfDot
                    "longDotId" ==> lid longDotId]
             Childs = [yield visitSynExpr expr]}
        | SynExpr.DotSet(expr,longDotId,e2,range) ->
            {Type = "SynExpr.DotSet"
             Range = r range
             Properties = p ["longDotId" ==> lid longDotId]
             Childs =
                 [yield visitSynExpr expr
                  yield visitSynExpr e2]}
        | SynExpr.Set(e1,e2,range) ->
            {Type = "SynExpr.Set"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynExpr e1
                  yield visitSynExpr e2]}
        | SynExpr.DotIndexedGet(objectExpr,indexExprs,dotRange,range) ->
            {Type = "SynExpr.DotIndexedGet"
             Range = r range
             Properties = p ["dotRange" ==> r dotRange]
             Childs =
                 [yield visitSynExpr objectExpr
                  yield! indexExprs |> List.map visitSynIndexerArg]}
        | SynExpr.DotIndexedSet(objectExpr,indexExprs,valueExpr,leftOfSetRange,dotRange,range) ->
            {Type = "SynExpr.DotIndexedSet"
             Range = r range
             Properties =
                 p ["leftOfSetRange" ==> r leftOfSetRange
                    "dotRange" ==> r dotRange]
             Childs =
                 [yield visitSynExpr objectExpr
                  yield! indexExprs |> List.map visitSynIndexerArg
                  yield visitSynExpr valueExpr]}
        | SynExpr.NamedIndexedPropertySet(longDotId,e1,e2,range) ->
            {Type = "SynExpr.NamedIndexedPropertySet"
             Range = r range
             Properties = p ["longDotId" ==> lid longDotId]
             Childs =
                 [yield visitSynExpr e1
                  yield visitSynExpr e2]}
        | SynExpr.DotNamedIndexedPropertySet(expr,longDotId,e1,e2,range) ->
            {Type = "SynExpr.DotNamedIndexedPropertySet"
             Range = r range
             Properties = p ["longDotId" ==> lid longDotId]
             Childs =
                 [yield visitSynExpr expr
                  yield visitSynExpr e1
                  yield visitSynExpr e2]}
        | SynExpr.TypeTest(expr,typeName,range) ->
            {Type = "SynExpr.TypeTest"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynExpr expr
                  yield visitSynType typeName]}
        | SynExpr.Upcast(expr,typeName,range) ->
            {Type = "SynExpr.Upcast"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynExpr expr
                  yield visitSynType typeName]}
        | SynExpr.Downcast(expr,typeName,range) ->
            {Type = "SynExpr.Downcast"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynExpr expr
                  yield visitSynType typeName]}
        | SynExpr.InferredUpcast(expr,range) ->
            {Type = "SynExpr.InferredUpcast"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}
        | SynExpr.InferredDowncast(expr,range) ->
            {Type = "SynExpr.InferredDowncast"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}
        | SynExpr.Null(range) ->
            {Type = "SynExpr.Null"
             Range = r range
             Properties = p []
             Childs = []}
        | SynExpr.AddressOf(isByref,expr,refRange,range) ->
            {Type = "SynExpr.AddressOf"
             Range = r range
             Properties =
                 p ["isByref" ==> isByref
                    "refRange" ==> r refRange]
             Childs = [yield visitSynExpr expr]}
        | SynExpr.TraitCall(typars,sign,expr,range) ->
            {Type = "SynExpr.AddressOf"
             Range = r range
             Properties = p []
             Childs =
                 [yield! typars |> List.map visitSynTypar
                  yield visitSynMemberSig sign
                  yield visitSynExpr expr]}
        | SynExpr.JoinIn(expr,inrange,expr2,range) ->
            {Type = "SynExpr.JoinIn"
             Range = r range
             Properties = p ["inRange" ==> r inrange]
             Childs =
                 [yield visitSynExpr expr
                  yield visitSynExpr expr2]}
        | SynExpr.ImplicitZero(range) ->
            {Type = "SynExpr.ImplicitZero"
             Range = r range
             Properties = p []
             Childs = []}
        | SynExpr.YieldOrReturn(_,expr,range) ->
            {Type = "SynExpr.YieldOrReturn"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}
        | SynExpr.YieldOrReturnFrom(_,expr,range) ->
            {Type = "SynExpr.YieldOrReturnFrom"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}
        | SynExpr.LetOrUseBang(_,isUse,isFromSource,pat,rhsExpr,body,range) ->
            {Type = "SynExpr.LetOrUseBang"
             Range = r range
             Properties =
                 p ["isUse" ==> isUse
                    "isFromSource" ==> isFromSource]
             Childs =
                 [yield visitSynPat pat
                  yield visitSynExpr rhsExpr
                  yield visitSynExpr body]}
        | SynExpr.MatchBang(_,expr,clauses,isExnMatch,range) ->
            {Type = "SynExpr.MatchBang"
             Range = r range
             Properties = p ["isExnMatch" ==> isExnMatch]
             Childs =
                 [yield visitSynExpr expr
                  yield! clauses |> List.map visitSynMatchClause]}
        | SynExpr.DoBang(expr,range) ->
            {Type = "SynExpr.DoBang"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}
        | SynExpr.LibraryOnlyILAssembly(_,_,_,_,range) ->
            {Type = "SynExpr.LibraryOnlyILAssembly"
             Range = r range
             Properties = p []
             Childs = []}
        | SynExpr.LibraryOnlyStaticOptimization(_,_,_,range) ->
            {Type = "SynExpr.LibraryOnlyStaticOptimization"
             Range = r range
             Properties = p []
             Childs = []}
        | SynExpr.LibraryOnlyUnionCaseFieldGet(expr,longId,_,range) ->
            {Type = "SynExpr.LibraryOnlyUnionCaseFieldGet"
             Range = r range
             Properties = p ["longId" ==> li longId]
             Childs = [yield visitSynExpr expr]}
        | SynExpr.LibraryOnlyUnionCaseFieldSet(e1,longId,_,e2,range) ->
            {Type = "SynExpr.LibraryOnlyUnionCaseFieldSet"
             Range = r range
             Properties = p ["longId" ==> li longId]
             Childs =
                 [yield visitSynExpr e1
                  yield visitSynExpr e2]}
        | SynExpr.ArbitraryAfterError(debugStr,range) ->
            {Type = "SynExpr.ArbitraryAfterError"
             Range = r range
             Properties = p ["debugStr" ==> debugStr]
             Childs = []}
        | SynExpr.FromParseError(expr,range) ->
            {Type = "SynExpr.FromParseError"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}
        | SynExpr.DiscardAfterMissingQualificationAfterDot(expr,range) ->
            {Type = "SynExpr.DiscardAfterMissingQualificationAfterDot"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}
        | SynExpr.Fixed(expr,range) ->
            {Type = "SynExpr.Fixed"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}

    and visitRecordField((longId,_): RecordFieldName,expr: SynExpr option,_: BlockSeparator option) =
        {Type = "RecordField"
         Range = noRange
         Properties = p ["ident" ==> lid longId]
         Childs =
             [if expr.IsSome then yield visitSynExpr expr.Value]}

    and visitSynMemberSig(ms: SynMemberSig): Node =
        match ms with
        | SynMemberSig.Member(valSig,_,range) ->
            {Type = "SynMemberSig.Member"
             Range = r range
             Properties = p []
             Childs = [yield visitSynValSig valSig]}
        | SynMemberSig.Interface(typeName,range) ->
            {Type = "SynMemberSig.Interface"
             Range = r range
             Properties = p []
             Childs = [yield visitSynType typeName]}
        | SynMemberSig.Inherit(typeName,range) ->
            {Type = "SynMemberSig.Inherit"
             Range = r range
             Properties = p []
             Childs = [yield visitSynType typeName]}
        | SynMemberSig.ValField(f,range) ->
            {Type = "SynMemberSig.ValField"
             Range = r range
             Properties = p []
             Childs = [yield visitSynField f]}
        | SynMemberSig.NestedType(typedef,range) ->
            {Type = "SynMemberSig.NestedType"
             Range = r range
             Properties = p []
             Childs = [yield visitSynTypeDefnSig typedef]}

    and visitSynIndexerArg(ia: SynIndexerArg): Node =
        match ia with
        | SynIndexerArg.One(e) ->
            {Type = "SynIndexerArg.One"
             Range = noRange
             Properties = p []
             Childs = [yield visitSynExpr e]}
        | SynIndexerArg.Two(e1,e2) ->
            {Type = "SynIndexerArg.Two"
             Range = noRange
             Properties = p []
             Childs =
                 [yield visitSynExpr e1
                  yield visitSynExpr e2]}

    and visitSynMatchClause(mc: SynMatchClause): Node =
        match mc with
        | SynMatchClause.Clause(pat,e1,e2,range,_) ->
            {Type = "RecordField"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynPat pat
                  if e1.IsSome then yield visitSynExpr e1.Value
                  yield visitSynExpr e2]}

    and visitArgsOption(expr: SynExpr,ident: Ident option) =
        {Type = "ArgOptions"
         Range = noRange
         Properties = p [if ident.IsSome then yield "ident" ==> i ident.Value]
         Childs = [yield visitSynExpr expr]}

    and visitSynInterfaceImpl(ii: SynInterfaceImpl): Node =
        match ii with
        | InterfaceImpl(typ,bindings,range) ->
            {Type = "InterfaceImpl"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynType typ
                  yield! (bindings |> List.map visitSynBinding)]}

    and visitSynTypeDefn(td: SynTypeDefn) =
        match td with
        | TypeDefn(sci,stdr,members,range) ->
            {Type = "TypeDefn"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynComponentInfo sci
                  yield visitSynTypeDefnRepr stdr
                  yield! (members |> List.map visitSynMemberDefn)]}

    and visitSynTypeDefnSig(typeDefSig: SynTypeDefnSig): Node =
        match typeDefSig with
        | TypeDefnSig(sci,_,memeberSig,range) ->
            {Type = "TypeDefnSig"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynComponentInfo sci
                  yield! (memeberSig |> List.map visitSynMemberSig)]}

    and visitSynMemberDefn(mbrDef: SynMemberDefn): Node =
        match mbrDef with
        | SynMemberDefn.Open(longIdent,range) ->
            {Type = "SynMemberDefn.Open"
             Range = r range
             Properties = p ["longIdent" ==> li longIdent]
             Childs = []}
        | SynMemberDefn.Member(memberDefn,range) ->
            {Type = "SynMemberDefn.Member"
             Range = r range
             Properties = p []
             Childs = [yield visitSynBinding memberDefn]}
        | SynMemberDefn.ImplicitCtor(access,attrs,ctorArgs,selfIdentifier,range) ->
            {Type = "SynMemberDefn.ImplicitCtor"
             Range = r range
             Properties =
                 p [if selfIdentifier.IsSome then yield "selfIdent" ==> i selfIdentifier.Value
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs =
                 [yield! attrs |> List.map visitSynAttribute
                  yield! (ctorArgs |> List.map visitSynSimplePat)]}
        | SynMemberDefn.ImplicitInherit(inheritType,inheritArgs,inheritAlias,range) ->
            {Type = "SynMemberDefn.ImplicitInherit"
             Range = r range
             Properties = p [if inheritAlias.IsSome then yield "inheritAlias" ==> i inheritAlias.Value]
             Childs =
                 [yield visitSynType inheritType
                  yield visitSynExpr inheritArgs]}
        | SynMemberDefn.LetBindings(bindings,isStatic,isRecursive,range) ->
            {Type = "SynMemberDefn.LetBindings"
             Range = r range
             Properties =
                 p ["isStatic" ==> isStatic
                    "isRecursive" ==> isRecursive]
             Childs = [yield! bindings |> List.map visitSynBinding]}
        | SynMemberDefn.AbstractSlot(valSig,memberFlag,range) ->
            {Type = "SynMemberDefn.AbstractSlot"
             Range = r range
             Properties = p []
             Childs = [yield visitSynValSig valSig]}
        | SynMemberDefn.Interface(typ,members,range) ->
            {Type = "SynMemberDefn.Interface"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynType typ
                  if members.IsSome then yield! members.Value |> List.map visitSynMemberDefn]}
        | SynMemberDefn.Inherit(typ,ident,range) ->
            {Type = "SynMemberDefn.Inherit"
             Range = r range
             Properties = p [if ident.IsSome then yield "ident" ==> i ident.Value]
             Childs = [yield visitSynType typ]}
        | SynMemberDefn.ValField(fld,range) ->
            {Type = "SynMemberDefn.ValField"
             Range = r range
             Properties = p []
             Childs = [yield visitSynField fld]}
        | SynMemberDefn.NestedType(typeDefn,access,range) ->
            {Type = "SynMemberDefn.NestedType"
             Range = r range
             Properties = p [if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs = [yield visitSynTypeDefn typeDefn]}
        | SynMemberDefn.AutoProperty(attrs,isStatic,ident,typeOpt,propKind,memberFlags,xmlDoc,access,synExpr,getSetRange,range) ->
            {Type = "SynMemberDefn.AutoProperty"
             Range = r range
             Properties =
                 p [yield "isStatic" ==> isStatic
                    yield "ident" ==> i ident
                    yield "propKind" ==> visitMemberKind propKind
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)
                    if getSetRange.IsSome then yield "getSetRange" ==> (getSetRange.Value |> r)]
             Childs =
                 [yield! attrs |> List.map visitSynAttribute
                  if typeOpt.IsSome then yield visitSynType typeOpt.Value
                  yield visitSynExpr synExpr]}

    and visitSynSimplePat(sp: SynSimplePat): Node =
        match sp with
        | SynSimplePat.Id(ident,_,isCompilerGenerated,isThisVar,isOptArg,range) ->
            {Type = "SynSimplePat.Id"
             Range = r range
             Properties =
                 p ["isCompilerGenerated" ==> isCompilerGenerated
                    "isThisVar" ==> isThisVar
                    "isOptArg" ==> isOptArg
                    "ident" ==> i ident]
             Childs = []}
        | SynSimplePat.Typed(simplePat,typ,range) ->
            {Type = "SynSimplePat.Typed"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynSimplePat simplePat
                  yield visitSynType typ]}
        | SynSimplePat.Attrib(simplePat,attrs,range) ->
            {Type = "SynSimplePat.Attrib"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynSimplePat simplePat
                  yield! attrs |> List.map visitSynAttribute]}

    and visitSynSimplePats(sp: SynSimplePats): Node =
        match sp with
        | SynSimplePats.SimplePats(pats,range) ->
            {Type = "SynSimplePats.SimplePats"
             Range = r range
             Properties = p []
             Childs = [yield! pats |> List.map visitSynSimplePat]}
        | SynSimplePats.Typed(pats,typ,range) ->
            {Type = "SynSimplePats.Typed"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynSimplePats pats
                  yield visitSynType typ]}

    and visitSynBinding(binding: SynBinding): Node =
        match binding with
        | Binding(access,kind,mustInline,isMutable,attrs,_,valData,headPat,returnInfo,expr,range,_) ->
            {Type = "Binding"
             Range = r range
             Properties =
                 p [yield "mustInline" ==> mustInline
                    yield "isMutable" ==> isMutable
                    yield "kind" ==> visitSynBindingKind kind
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs =
                 [yield! attrs |> List.map visitSynAttribute
                  yield visitSynValData valData
                  yield visitSynPat headPat
                  if returnInfo.IsSome then yield visitSynBindingReturnInfo returnInfo.Value
                  yield visitSynExpr expr]}

    and visitSynValData(svd: SynValData): Node =
        match svd with
        | SynValData(_,svi,ident) ->
            {Type = "Binding"
             Range = noRange
             Properties = p [ if ident.IsSome then yield "ident" ==> (ident.Value |> i)]
             Childs = [yield visitSynValInfo svi]}

    and visitSynValSig(svs: SynValSig): Node =
        match svs with
        | ValSpfn(attrs,ident,explicitValDecls,synType,arity,isInline,isMutable,_,access,expr,range) ->
            {Type = "ValSpfn"
             Range = r range
             Properties =
                 p [yield "ident" ==> i ident
                    yield "isMutable" ==> isMutable
                    yield "isInline" ==> isInline
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs =
                 [yield! attrs |> List.map visitSynAttribute
                  yield visitSynValTyparDecls explicitValDecls
                  yield visitSynType synType
                  yield visitSynValInfo arity
                  if expr.IsSome then yield visitSynExpr expr.Value]}

    and visitSynValTyparDecls(valTypeDecl: SynValTyparDecls): Node =
        match valTypeDecl with
        | SynValTyparDecls(typardecls,_,_) ->
            {Type = "SynValTyparDecls"
             Range = noRange
             Properties = p []
             Childs = [yield! typardecls |> List.map visitSynTyparDecl]}

    and visitSynTyparDecl(std: SynTyparDecl): Node =
        match std with
        | TyparDecl(attrs,typar) ->
            {Type = "TyparDecl"
             Range = noRange
             Properties = p []
             Childs =
                 [yield! attrs |> List.map visitSynAttribute
                  yield visitSynTypar typar]}

    and visitSynTypar(typar: SynTypar): Node =
        match typar with
        | Typar(ident,staticReq,isComGen) ->
            {Type = "ValSpfn"
             Range = noRange
             Properties =
                 p ["ident" ==> i ident
                    "isComGen" ==> isComGen
                    "staticReq" ==> visitTyparStaticReq staticReq]
             Childs = []}

    and visitTyparStaticReq(tsr: TyparStaticReq) =
        match tsr with
        | NoStaticReq -> "NoStaticReq"
        | HeadTypeStaticReq -> "HeadTypeStaticReq"

    and visitSynBindingReturnInfo(returnInfo: SynBindingReturnInfo): Node =
        match returnInfo with
        | SynBindingReturnInfo(typeName,range,attrs) ->
            {Type = "ComponentInfo"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynType typeName
                  yield! (attrs |> List.map visitSynAttribute)]}

    and visitSynPat(sp: SynPat): Node =
        match sp with
        | SynPat.Const(sc,range) ->
            {Type = "SynPat.Const"
             Range = r range
             Properties = p ["const" ==> visitSynConst sc]
             Childs = []}
        | SynPat.Wild(range) ->
            {Type = "SynPat.Wild"
             Range = r range
             Properties = p []
             Childs = []}
        | SynPat.Named(synPat,ident,isSelfIdentifier,access,range) ->
            {Type = "SynPat.Named"
             Range = r range
             Properties =
                 p [yield "ident" ==> i ident
                    yield "isSelfIdentifier" ==> isSelfIdentifier
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs = [yield visitSynPat synPat]}
        | SynPat.Typed(synPat,synType,range) ->
            {Type = "SynPat.Typed"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynPat synPat
                  yield visitSynType synType]}
        | SynPat.Attrib(synPat,attrs,range) ->
            {Type = "SynPat.Attrib"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynPat synPat
                  yield! attrs |> List.map visitSynAttribute]}
        | SynPat.Or(synPat,synPat2,range) ->
            {Type = "SynPat.Or"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynPat synPat
                  yield visitSynPat synPat2]}
        | SynPat.Ands(pats,range) ->
            {Type = "SynPat.Ands"
             Range = r range
             Properties = p []
             Childs = [yield! pats |> List.map visitSynPat]}
        | SynPat.LongIdent(longDotId,ident,svtd,ctorArgs,access,range) ->
            {Type = "SynPat.LongIdent"
             Range = r range
             Properties =
                 p [if ident.IsSome then yield "ident" ==> (ident.Value |> i)
                    yield "longDotId" ==> lid longDotId
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs =
                 [if svtd.IsSome then yield visitSynValTyparDecls svtd.Value
                  yield visitSynConstructorArgs ctorArgs]}
        | SynPat.Tuple(pats,range) ->
            {Type = "SynPat.Tuple"
             Range = r range
             Properties = p []
             Childs = [yield! pats |> List.map visitSynPat]}
        | SynPat.StructTuple(pats,range) ->
            {Type = "SynPat.StructTuple"
             Range = r range
             Properties = p []
             Childs = [yield! pats |> List.map visitSynPat]}
        | SynPat.Paren(pat,range) ->
            {Type = "SynPat.Paren"
             Range = r range
             Properties = p []
             Childs = [visitSynPat pat]}
        | SynPat.ArrayOrList(_,pats,range) ->
            {Type = "SynPat.ArrayOrList"
             Range = r range
             Properties = p []
             Childs = [yield! pats |> List.map visitSynPat]}
        | SynPat.Record(pats,range) ->
            {Type = "SynPat.Record"
             Range = r range
             Properties = p []
             Childs = [yield! pats |> List.map(snd >> visitSynPat)]}
        | SynPat.Null(range) ->
            {Type = "SynPat.Null"
             Range = r range
             Properties = p []
             Childs = []}
        | SynPat.OptionalVal(ident,range) ->
            {Type = "SynPat.OptionalVal"
             Range = r range
             Properties = p ["ident" ==> i ident]
             Childs = []}
        | SynPat.IsInst(typ,range) ->
            {Type = "SynPat.IsInst"
             Range = r range
             Properties = p []
             Childs = [visitSynType typ]}
        | SynPat.QuoteExpr(expr,range) ->
            {Type = "SynPat.QuoteExpr"
             Range = r range
             Properties = p []
             Childs = [visitSynExpr expr]}
        | SynPat.DeprecatedCharRange(c,c2,range) ->
            {Type = "SynPat.DeprecatedCharRange"
             Range = r range
             Properties =
                 p ["c" ==> c
                    "c2" ==> c2]
             Childs = []}
        | SynPat.InstanceMember(ident,ident2,ident3,access,range) ->
            {Type = "SynPat.InstanceMember"
             Range = r range
             Properties =
                 p [yield "ident" ==> i ident
                    yield "ident2" ==> i ident2
                    if ident3.IsSome then yield "ident3" ==> (ident3.Value |> i)
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs = []}
        | SynPat.FromParseError(pat,range) ->
            {Type = "SynPat.FromParseError"
             Range = r range
             Properties = p []
             Childs = [visitSynPat pat]}

    and visitSynConstructorArgs(ctorArgs: SynConstructorArgs): Node =
        match ctorArgs with
        | Pats(pats) ->
            {Type = "Pats"
             Range = noRange
             Properties = p []
             Childs = [yield! pats |> List.map visitSynPat]}
        | NamePatPairs(pats,range) ->
            {Type = "NamePatPairs"
             Range = r range
             Properties = p []
             Childs = [yield! pats |> List.map(snd >> visitSynPat)]}

    and visitSynComponentInfo(sci: SynComponentInfo): Node =
        match sci with
        | ComponentInfo(attribs,typeParams,_,longId,_,preferPostfix,access,range) ->
            {Type = "ComponentInfo"
             Range = r range
             Properties =
                 p [yield "longIdent" ==> li longId
                    yield "preferPostfix" ==> preferPostfix
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs =
                 [yield! (attribs |> List.map visitSynAttribute)
                  yield! (typeParams |> List.map(visitSynTyparDecl))]}

    and visitSynTypeDefnRepr(stdr: SynTypeDefnRepr): Node =
        match stdr with
        | SynTypeDefnRepr.ObjectModel(kind,members,range) ->
            {Type = "SynTypeDefnRepr.ObjectModel"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynTypeDefnKind kind
                  yield! (members |> List.map visitSynMemberDefn)]}
        | SynTypeDefnRepr.Simple(simpleRepr,range) ->
            {Type = "SynTypeDefnRepr.ObjectModel"
             Range = r range
             Properties = p []
             Childs = [yield visitSynTypeDefnSimpleRepr simpleRepr]}
        | SynTypeDefnRepr.Exception(exceptionRepr) ->
            {Type = "SynTypeDefnRepr.Exception"
             Range = noRange
             Properties = p []
             Childs = [yield visitSynExceptionDefnRepr exceptionRepr]}

    and visitSynTypeDefnKind(kind: SynTypeDefnKind) =
        match kind with
        | TyconUnspecified ->
            {Type = "SynTypeDefnKind.TyconUnspecified"
             Range = noRange
             Properties = p []
             Childs = []}
        | TyconClass ->
            {Type = "SynTypeDefnKind.TyconClass"
             Range = noRange
             Properties = p []
             Childs = []}
        | TyconInterface ->
            {Type = "SynTypeDefnKind.TyconInterface"
             Range = noRange
             Properties = p []
             Childs = []}
        | TyconStruct ->
            {Type = "SynTypeDefnKind.TyconStruct"
             Range = noRange
             Properties = p []
             Childs = []}
        | TyconRecord ->
            {Type = "SynTypeDefnKind.TyconRecord"
             Range = noRange
             Properties = p []
             Childs = []}
        | TyconUnion ->
            {Type = "SynTypeDefnKind.TyconUnion"
             Range = noRange
             Properties = p []
             Childs = []}
        | TyconAbbrev ->
            {Type = "SynTypeDefnKind.TyconAbbrev"
             Range = noRange
             Properties = p []
             Childs = []}
        | TyconHiddenRepr ->
            {Type = "SynTypeDefnKind.TyconHiddenRepr"
             Range = noRange
             Properties = p []
             Childs = []}
        | TyconAugmentation ->
            {Type = "SynTypeDefnKind.TyconAugmentation"
             Range = noRange
             Properties = p []
             Childs = []}
        | TyconILAssemblyCode ->
            {Type = "SynTypeDefnKind.TyconILAssemblyCode"
             Range = noRange
             Properties = p []
             Childs = []}
        | TyconDelegate(typ,valinfo) ->
            {Type = "SynTypeDefnKind.TyconDelegate"
             Range = noRange
             Properties = p []
             Childs =
                 [yield visitSynType typ
                  yield visitSynValInfo valinfo]}

    and visitSynTypeDefnSimpleRepr(arg: SynTypeDefnSimpleRepr) =
        match arg with
        | SynTypeDefnSimpleRepr.None(range) ->
            {Type = "SynTypeDefnSimpleRepr.None"
             Range = r range
             Properties = p []
             Childs = []}
        | SynTypeDefnSimpleRepr.Union(access,unionCases,range) ->
            {Type = "SynTypeDefnSimpleRepr.Union"
             Range = r range
             Properties = p [if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs = [yield! unionCases |> List.map visitSynUnionCase]}
        | SynTypeDefnSimpleRepr.Enum(enumCases,range) ->
            {Type = "SynTypeDefnSimpleRepr.Enum"
             Range = r range
             Properties = p []
             Childs = [yield! enumCases |> List.map visitSynEnumCase]}
        | SynTypeDefnSimpleRepr.Record(access,recordFields,range) ->
            {Type = "SynTypeDefnSimpleRepr.Record"
             Range = r range
             Properties = p [if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs = [yield! recordFields |> List.map visitSynField]}
        | SynTypeDefnSimpleRepr.General(_,_,_,_,_,_,_,range) ->
            {Type = "SynTypeDefnSimpleRepr.General"
             Range = r range
             Properties = p []
             Childs = []}
        | SynTypeDefnSimpleRepr.LibraryOnlyILAssembly(_,range) ->
            {Type = "SynTypeDefnSimpleRepr.LibraryOnlyILAssembly"
             Range = r range
             Properties = p []
             Childs = []}
        | SynTypeDefnSimpleRepr.TypeAbbrev(_,typ,range) ->
            {Type = "SynTypeDefnSimpleRepr.TypeAbbrev"
             Range = r range
             Properties = p []
             Childs = [visitSynType typ]}
        | SynTypeDefnSimpleRepr.Exception(edr) ->
            {Type = "SynTypeDefnSimpleRepr.Exception"
             Range = noRange
             Properties = p []
             Childs = [visitSynExceptionDefnRepr edr]}

    and visitSynExceptionDefn(exceptionDef: SynExceptionDefn): Node =
        match exceptionDef with
        | SynExceptionDefn(sedr,members,range) ->
            {Type = "SynExceptionDefn"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynExceptionDefnRepr sedr
                  yield! (members |> List.map visitSynMemberDefn)]}

    and visitSynExceptionDefnRepr(sedr: SynExceptionDefnRepr): Node =
        match sedr with
        | SynExceptionDefnRepr(attrs,unionCase,longId,_,access,range) ->
            {Type = "SynExceptionDefnRepr"
             Range = r range
             Properties =
                 p [if longId.IsSome then yield "longIdent" ==> (longId.Value |> li)
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs =
                 [yield! attrs |> List.map visitSynAttribute
                  yield visitSynUnionCase unionCase]}

    and visitSynAttribute(attr: SynAttribute): Node =
        {Type = "SynAttribute"
         Range = r attr.Range
         Properties =
             p [if attr.Target.IsSome then yield "target" ==> i attr.Target.Value
                yield "typeName" ==> lid attr.TypeName
                yield "appliesToGetterAndSetter" ==> attr.AppliesToGetterAndSetter
                yield "typeName" ==> lid attr.TypeName]
         Childs = [visitSynExpr attr.ArgExpr]}

    and visitSynUnionCase(uc: SynUnionCase): Node =
        match uc with
        | UnionCase(attrs,ident,uct,_,access,range) ->
            {Type = "UnionCase"
             Range = r range
             Properties =
                 p [yield "ident" ==> i ident
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs =
                 [yield visitSynUnionCaseType uct
                  yield! attrs |> List.map visitSynAttribute]}

    and visitSynUnionCaseType(uct: SynUnionCaseType) =
        match uct with
        | UnionCaseFields(cases) ->
            {Type = "UnionCaseFields"
             Range = noRange
             Properties = p []
             Childs = [yield! cases |> List.map visitSynField]}
        | UnionCaseFullType(stype,valInfo) ->
            {Type = "UnionCaseFullType"
             Range = noRange
             Properties = p []
             Childs =
                 [yield visitSynType stype
                  yield visitSynValInfo valInfo]}

    and visitSynEnumCase(sec: SynEnumCase): Node =
        match sec with
        | EnumCase(attrs,ident,_,_,range) ->
            {Type = "EnumCase"
             Range = r range
             Properties = p ["ident" ==> i ident]
             Childs = [yield! attrs |> List.map visitSynAttribute]}

    and visitSynField(sfield: SynField): Node =
        match sfield with
        | Field(attrs,isStatic,ident,typ,_,_,access,range) ->
            {Type = "Field"
             Range = r range
             Properties =
                 p [if ident.IsSome then yield "ident" ==> (ident.Value |> i)
                    yield "isStatic" ==> isStatic
                    if access.IsSome then yield "access" ==> (access.Value |> visitSynAccess)]
             Childs =
                 [yield! attrs |> List.map visitSynAttribute
                  yield visitSynType typ]}

    and visitSynType(st: SynType) =
        match st with
        | SynType.LongIdent(li) ->
            {Type = "SynType.LongIdent"
             Range = noRange
             Properties = p ["ident" ==> lid li]
             Childs = []}
        | SynType.App(typeName,lESSrange,typeArgs,commaRanges,gREATERrange,isPostfix,range) ->
            {Type = "SynType.App"
             Range = r range
             Properties =
                 p [if lESSrange.IsSome then yield "lESSrange" ==> (lESSrange.Value |> r)
                    yield "commaRanges" ==> (commaRanges |> List.map r)
                    if gREATERrange.IsSome then yield "gREATERrange" ==> (gREATERrange.Value |> r)
                    yield "isPostfix" ==> isPostfix]
             Childs =
                 [yield! typeArgs |> List.map visitSynType
                  yield visitSynType typeName]}
        | SynType.LongIdentApp(typeName,longDotId,lESSRange,typeArgs,commaRanges,gREATERrange,range) ->
            {Type = "SynType.LongIdentApp"
             Range = r range
             Properties =
                 p [yield "ident" ==> lid longDotId
                    if lESSRange.IsSome then yield "lESSRange" ==> (lESSRange.Value |> r)
                    yield "commaRanges" ==> (commaRanges |> List.map r)
                    if gREATERrange.IsSome then yield "gREATERrange" ==> (gREATERrange.Value |> r)]
             Childs =
                 [yield! typeArgs |> List.map visitSynType
                  yield visitSynType typeName]}
        | SynType.Tuple(typeNames,range) ->
            {Type = "SynType.Tuple"
             Range = r range
             Properties = p []
             Childs = [yield! typeNames |> List.map(snd >> visitSynType)]}
        | SynType.StructTuple(typeNames,range) ->
            {Type = "SynType.StructTuple"
             Range = r range
             Properties = p []
             Childs = [yield! typeNames |> List.map(snd >> visitSynType)]}
        | SynType.Array(_,elementType,range) ->
            {Type = "SynType.Array"
             Range = r range
             Properties = p []
             Childs = [yield visitSynType elementType]}
        | SynType.Fun(argType,returnType,range) ->
            {Type = "SynType.Fun"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynType argType
                  yield visitSynType returnType]}
        | SynType.Var(genericName,range) ->
            {Type = "SynType.Var"
             Range = r range
             Properties = p []
             Childs = [yield visitSynTypar genericName]}
        | SynType.Anon(range) ->
            {Type = "SynType.Anon"
             Range = r range
             Properties = p []
             Childs = []}
        | SynType.WithGlobalConstraints(typeName,_,range) ->
            {Type = "SynType.WithGlobalConstraints"
             Range = r range
             Properties = p []
             Childs = [yield visitSynType typeName]}
        | SynType.HashConstraint(synType,range) ->
            {Type = "SynType.HashConstraint"
             Range = r range
             Properties = p []
             Childs = [yield visitSynType synType]}
        | SynType.MeasureDivide(dividendType,divisorType,range) ->
            {Type = "SynType.MeasureDivide"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynType dividendType
                  yield visitSynType divisorType]}
        | SynType.MeasurePower(measureType,_,range) ->
            {Type = "SynType.MeasurePower"
             Range = r range
             Properties = p []
             Childs = [yield visitSynType measureType]}
        | SynType.StaticConstant(constant,range) ->
            {Type = "SynType.StaticConstant"
             Range = r range
             Properties = p ["constant" ==> visitSynConst constant]
             Childs = []}
        | SynType.StaticConstantExpr(expr,range) ->
            {Type = "SynType.StaticConstantExpr"
             Range = r range
             Properties = p []
             Childs = [yield visitSynExpr expr]}
        | SynType.StaticConstantNamed(expr,typ,range) ->
            {Type = "SynType.StaticConstantNamed"
             Range = r range
             Properties = p []
             Childs =
                 [yield visitSynType expr
                  yield visitSynType typ]}

    and visitSynConst(sc: SynConst) = sprintf "%A" sc

    and visitSynValInfo(svi: SynValInfo) =
        match svi with
        | SynValInfo(args,arg) ->
            {Type = "SynValInfo"
             Range = noRange
             Properties = p []
             Childs =
                 [yield! args |> List.collect(List.map visitSynArgInfo)
                  yield visitSynArgInfo arg]}

    and visitSynArgInfo(sai: SynArgInfo) =
        match sai with
        | SynArgInfo(attrs,optional,ident) ->
            {Type = "SynArgInfo"
             Range = noRange
             Properties =
                 p [if ident.IsSome then yield "ident" ==> i ident.Value
                    yield "optional" ==> optional]
             Childs = [yield! attrs |> List.map visitSynAttribute]}

    and visitSynAccess(a: SynAccess) =
        match a with
        | SynAccess.Private -> "Private"
        | SynAccess.Internal -> "Internal"
        | SynAccess.Public -> "Public"

    and visitSynBindingKind(kind: SynBindingKind) =
        match kind with
        | SynBindingKind.DoBinding -> "Do Binding"
        | SynBindingKind.StandaloneExpression -> "Standalone Expression"
        | SynBindingKind.NormalBinding -> "Normal Binding"

    and visitMemberKind(mk: MemberKind) =
        match mk with
        | MemberKind.ClassConstructor -> "ClassConstructor"
        | MemberKind.Constructor -> "Constructor"
        | MemberKind.Member -> "Member"
        | MemberKind.PropertyGet -> "PropertyGet"
        | MemberKind.PropertySet -> "PropertySet"
        | MemberKind.PropertyGetSet -> "PropertyGetSet"

    and visitParsedHashDirective(hash: ParsedHashDirective): Node =
        match hash with
        | ParsedHashDirective(ident,longIdent,range) ->
            {Type = "ParsedHashDirective"
             Range = r range
             Properties =
                 p ["ident" ==> ident
                    "longIdent" ==> longIdent]
             Childs = []}

let astToNode fl (ast: SynModuleDecls): Node =
    let child = ast |> List.map Ast.visit
    {Type = "File"
     Range = None
     Properties = Map.empty
     Childs = child}
