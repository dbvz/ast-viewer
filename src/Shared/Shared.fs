namespace Shared
open System.Collections.Generic

module Const =
    let sourceSizeLimit = 10 * 1024

type Range =
    { StartLine: int
      StartCol: int
      EndLine: int
      EndCol: int }

type Id = {
    Ident: string
    Range: Range option
}

type Node = {
    Type: string
    Range: Range option
    Properties: Map<string, obj>
    Childs: Node list
}

type Dto = {
    NodeJson: string
    String: string
}

type EditorState =
    | Loading
    | Loaded

type View =
    | Editor
    | JsonViewer
    | Raw

type Model =
    { Source : string
      Parsed : Result<Dto option, string>
      IsLoading : bool
      Version : string
      View: View
      FSharpEditorState : EditorState }

    static member Default =
                            { Source = ""
                              Parsed = Ok None
                              IsLoading = false
                              Version = ""
                              View = Editor
                              FSharpEditorState = Loading }



module Route =
    /// Defines how routes are generated on server and mapped from client
    let builder typeName methodName =
        sprintf "/api/ast/%s/%s" typeName methodName

/// A type that specifies the communication protocol between client and server
/// to learn more, read the docs at https://zaid-ajaj.github.io/Fable.Remoting/src/basics.html
type IModelApi =
    {
        init : unit -> Async<Model>
        version : unit -> Async<string>
        parse : string -> Async<Result<Dto, string>>
        typeCheck: string -> Async<Result<Dto, string>>
    }
